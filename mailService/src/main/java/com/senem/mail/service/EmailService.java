package com.senem.mail.service;

import com.senem.mail.model.Mail;

public interface EmailService {

    void sendMimeMessage(final Mail x);
}
