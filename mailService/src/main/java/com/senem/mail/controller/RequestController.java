package com.senem.mail.controller;

import com.senem.mail.model.Mail;
import com.senem.mail.service.EmailServiceImplementation;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/sendMail")
@NoArgsConstructor
public class RequestController {

    @Autowired
    private EmailServiceImplementation emailService;

    private static final Logger logger = LoggerFactory.getLogger(RequestController.class);

    @RequestMapping(method = POST, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getInfo(@RequestBody Mail mail){

        emailService.sendMimeMessage(mail);

        return ResponseEntity.ok("It is ok.");
    }
}
