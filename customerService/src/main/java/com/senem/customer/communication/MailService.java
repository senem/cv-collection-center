package com.senem.customer.communication;

import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class MailService {

    public MailService(){
        From = "senemumac@gmail.com";
        To = "senemumac.91@gmail.com";
        Subject = "ss";
        Content = "<h2>Hello World!</h2>";

        input = String.format("{ \"from\": \"%s\", \"to\": \"%s\", \"subject\": \"%s\", \"content\": \"%s\"}", From, To, Subject, Content);
}

    private final String uri = "http://localhost:8300/api/sendMail";;

    private String From = null;
    private String To = null;
    private String Subject = null;
    private String Content = null;
    private String input = null;

    public void send() {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(input, headers);

        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);

        System.out.println(response);
    }
}
